package edu.bbte.idde.keim1831.web;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "TripServlet",
        initParams = {
                @WebInitParam(name = "initial", value = "42")
        },
        urlPatterns = {"/trips"})
public class TripServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TripServlet.class);
    transient TripDAO tripDao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing example servlet");
        LOG.info("Initial param: {}", config.getInitParameter("initial"));
        tripDao = DaoContext.INSTANCE.getTripDao();
    }

    @Override
    public void destroy() {
        super.destroy();
        LOG.info("Destroying example servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info("Request arrived to servlet");
        PrintWriter writer = resp.getWriter();
        String reqId = req.getParameter("id");
        if (reqId == null) {
            for (Trip trip : tripDao.getAll()) {
                writer.println(trip.toString());
            }
        } else {
            try {
                int id = Integer.parseInt(reqId);
                Trip trip = tripDao.getById(id);
                writer.println(trip);

            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            } catch (EntityNotFoundException e) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Post request arrived");
        Trip trip = new Trip();
        String tripName = req.getParameter("name");
        String tripDescription = req.getParameter("description");
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date tripStart = sdf.parse(req.getParameter("start"));

            String tripMeetingPoint = req.getParameter("meetingPoint");

            if (tripName == null || tripDescription == null || tripStart == null || tripMeetingPoint == null) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                trip.setName(tripName);
                trip.setDescription(tripDescription);
                trip.setStart(tripStart);
                trip.setMeetingPoint(tripMeetingPoint);
                tripDao.create(trip);
                var writer = resp.getWriter();
                writer.println("Created.");
                resp.setStatus(HttpServletResponse.SC_CREATED);
            }
        } catch (Exception e) {
            LOG.info(e.toString());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}