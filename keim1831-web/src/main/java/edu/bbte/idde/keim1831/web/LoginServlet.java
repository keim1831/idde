package edu.bbte.idde.keim1831.web;

import com.github.jknack.handlebars.Template;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet",
        initParams = {
                @WebInitParam(name = "initial", value = "42")
        },
        urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class);
    TripDAO tripDao;
    private final String user = "user1";
    private final String passw = "alma";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing login servlet");
        LOG.info("Initial param: {}", config.getInitParameter("initial"));
        tripDao = DaoContext.INSTANCE.getTripDao();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Template template = HandlebarsTemplateFactory.getTemplate("login");
        template.apply(null, resp.getWriter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOG.info("Login post request arrived");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        LOG.info(username);
        LOG.info(password);

        if (user.equals(username) && passw.equals(password)) {
            HttpSession session = request.getSession();
            session.setAttribute("username",user);
            response.sendRedirect("trips-html");
        } else {
            response.sendRedirect("login");
        }
    }
}