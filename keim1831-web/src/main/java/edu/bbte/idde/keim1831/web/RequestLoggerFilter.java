package edu.bbte.idde.keim1831.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class RequestLoggerFilter extends HttpFilter {

    private static Logger LOGGER = LoggerFactory.getLogger(RequestLoggerFilter.class);

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        chain.doFilter(req, res);
        LOGGER.info("{}, method, {}, url, {}, statuscode", req.getMethod(), req.getRequestURI(), res.getStatus());
    }
}
