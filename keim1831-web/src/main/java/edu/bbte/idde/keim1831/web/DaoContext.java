package edu.bbte.idde.keim1831.web;

import edu.bbte.idde.keim1831.backend.dao.DaoFactory;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.model.Trip;

import java.util.Calendar;
import java.util.Date;

public class DaoContext {
    public static final DaoContext INSTANCE = new DaoContext();
    private TripDAO tripDao;

    private DaoContext() {
        tripDao = DaoFactory.getInstance().getTripDao();;
        tripDao.create(new Trip(
                0,
                "Name1",
                "desc1",
                new Date(2020, Calendar.OCTOBER, 21),
                "meetingPoint1"));
        tripDao.create(new Trip(
                0,
                "Name2",
                "desc2",
                new Date(2020, Calendar.OCTOBER, 27),
                "meetinPoint2"));
        tripDao.create(new Trip(
                0,
                "Name3",
                "desc3",
                new Date(2020, Calendar.NOVEMBER, 10),
                "meetingPoint3"));
    }

    public TripDAO getTripDao() {
        return tripDao;
    }
}
