package edu.bbte.idde.keim1831.web;

import com.github.jknack.handlebars.Template;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TripControllerServlet",
        initParams = {
                @WebInitParam(name = "initial", value = "42")
        },
        urlPatterns = {"/trips-html"})
public class TripControllerServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TripServlet.class);
    TripDAO tripDao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing example servlet");
        LOG.info("Initial param: {}", config.getInitParameter("initial"));
        tripDao = DaoContext.INSTANCE.getTripDao();
    }

    @Override
    public void destroy() {
        super.destroy();
        LOG.info("Destroying example servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Request arrived to example servlet");

        var trips = tripDao.getAll();

        // rendering
        Template template = HandlebarsTemplateFactory.getTemplate("trips");
        template.apply(trips, resp.getWriter());
    }
}