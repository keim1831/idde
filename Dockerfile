
# 1. fázis:
FROM gradle:6.7.0-jdk15 AS buildContainer
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle war --no-daemon


# 2. fázis:
FROM tomcat:9.0.39-jdk15-openjdk-slim-buster
COPY --from=buildContainer /home/gradle/src/keim1831-web/build/libs/*.war /usr/local/tomcat/webapps/ROOT.war
COPY ./docker-set-env.sh /usr/local/tomcat/bin/setenv.sh



