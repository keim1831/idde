package edu.bbte.idde.keim1831.desktop;

import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import java.awt.BorderLayout;
import java.awt.Color;

public class MyFrame extends JFrame {

    private TripDAO dao;
    private JTable table;
    private static Logger LOGGER = LoggerFactory.getLogger(MyFrame.class);
    public static final int WIDTH_OF_THE_FRAME = 750;
    public static final int HEIGHT_OF_THE_FRAME = 500;
    public static final int WIDTH_OF_THE_TOP = 10;
    public static final int WIDTH_OF_THE_LEFT = 10;
    public static final int WIDTH_OF_THE_BOTTOM = 10;
    public static final int WIDTH_OF_THE_RIGHT = 10;
    public static final int HGAP = 15;
    public static final int VGAP = 15;
    public static final int TABLE_BORDER_TOP = 1;
    public static final int TABLE_BORDER_LEFT = 1;
    public static final int TABLE_BORDER_BOTTOM = 1;
    public static final int TABLE_BORDER_RIGHT = 1;

    public MyFrame(final TripDAO d) {
        super("Trips");
        this.dao = d;
        setSize(WIDTH_OF_THE_FRAME, HEIGHT_OF_THE_FRAME);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        buildUI();
    }

    private void buildUI() {
        var pane = getContentPane();
        var panel = new JPanel();
        pane.add(panel);
        panel.setBorder(BorderFactory.createEmptyBorder(
                WIDTH_OF_THE_TOP,
                WIDTH_OF_THE_LEFT,
                WIDTH_OF_THE_BOTTOM,
                WIDTH_OF_THE_RIGHT)
        );
        panel.setLayout(new BorderLayout(HGAP, VGAP));
        var getButton = new JButton("GET");
        getButton.addActionListener((actionEvent) -> refreshData(panel));
        panel.add(getButton, BorderLayout.NORTH);
    }

    private void refreshData(final JPanel panel) {
        var trips = dao.getAll();
        LOGGER.info("Fetched " + trips.size() + " trips.");

        if (table != null) {
            panel.remove(table);
        }

        String[] columns = new String[] {
                "Id", "Name", "Description", "Start", "Meeting point"
        };

        //actual data for the table in a 2d array
        Object[][] data = new Object[trips.size()][];
        for (int i = 0; i < trips.size(); i++) {
            var trip = trips.get(i);
            data[i] = new Object[]{
                    trip.getId(),
                    trip.getName(),
                    trip.getDescription(),
                    trip.getStart(),
                    trip.getMeetingPoint()
            };
        }
        //create table with data
        table = new JTable(data, columns);

        table.setBorder(
                BorderFactory.createMatteBorder(
                        TABLE_BORDER_TOP,
                        TABLE_BORDER_LEFT,
                        TABLE_BORDER_BOTTOM,
                        TABLE_BORDER_RIGHT,
                        Color.BLACK)
        );
        panel.add(table, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
}
