package edu.bbte.idde.keim1831.desktop;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws EntityNotFoundException {

        LOGGER.info("Program started.");
        //var dao =  DaoFactory.getInstance().getOrganizerDao();
        //var dao2 = DaoFactory.getInstance().getTripDao();
        //var frame = new MyFrame(dao);
        //frame.setVisible(true);
        /*Trip trip = new Trip(
                0,
                "Name1",
                "desc1",
                new Date(2020, Calendar.OCTOBER, 21),
                "meetingPoint1");
        */

        /*//Select all from organiyers
        var a = dao.getAll();
        */

        /*//Select trips with organizer (id)
        var b = dao.selectTripsWithOrganizer(1);
        */

        /*//Select  organizer by organizer phone number
        var c =  dao.selectByPhoneNumber("+04753932838");
        */

        /*//Select trips by name
        var d = dao2.selectByName("Trip1");
        */

        /*//Get phone numbers (organizer id)
        var e = dao2.getOrganizersPhoneNumber(2);
        */

        /*//Delete from organizers
        dao2.delete(1);
        */

        /*//Delete from trips
        dao.delete(2);
        */

        /*//Update
        var organizer = dao.getById(1);
        var trip = organizer.getTrips().get(0);
        trip.setName(trip.getName() + 'z');
        dao.update(organizer);
        */
    }
}