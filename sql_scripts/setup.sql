CREATE DATABASE IF NOT EXISTS idde;
USE idde;

CREATE TABLE IF NOT EXISTS organizers
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    phoneNumber VARCHAR(12)
);

CREATE TABLE IF NOT EXISTS trips
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30),
    description VARCHAR(100),
    dateTime TIMESTAMP,
    meetingPoint VARCHAR(70),
    organizerId INT,
    FOREIGN KEY (organizerId) REFERENCES organizers (id)
);

INSERT INTO organizers(id, name, phoneNumber) VALUES
    (1,'Kiss Alma', '+04753932838'),
    (2,'Antal Norbert', '+04753982838'),
    (3, 'Nagy Ilona', '+04759932838');

INSERT INTO trips(name, description, dateTime, meetingPoint, organizerId) VALUES
    ('Trip1', 'description1','2020-11-25 14:00:00', 'Meses', 1),
    ('Trip2', 'description2','2021-05-12 14:00:00', 'Porolissum', 2),
    ('Trip3', 'description3','2020-12-06 14:00:00', 'Les', 1);

