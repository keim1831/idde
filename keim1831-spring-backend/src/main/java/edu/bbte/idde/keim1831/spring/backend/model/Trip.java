package edu.bbte.idde.keim1831.spring.backend.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trips")
public class Trip extends BaseEntity {
    @Column(length = 30)
    private String name;
    @Column(length = 100)
    private String description;

    @Column(name = "dateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Column(length = 70)
    private String meetingPoint;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "organizerId", referencedColumnName = "id")
    private Organizer organizer;

    public Trip() {
    }

    public Trip(int id, String name, String description, Date start, String meetingPoint) {
        super(id);
        this.name = name;
        this.description = description;
        this.start = start;
        this.meetingPoint = meetingPoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public String getMeetingPoint() {
        return meetingPoint;
    }

    public void setMeetingPoint(String meetingPoint) {
        this.meetingPoint = meetingPoint;
    }

    public Organizer getOrganizer() {
        return organizer;
    }

    public void setOrganizer(Organizer organizer) {
        this.organizer = organizer;
    }

    @Override
    public String toString() {
        return "Trip{"
            + "id="
            + getId()
            + ", name='"
            + name
            + "', description='"
            + description
            + "', start="
            + start
            + ", meetingPoint='"
            + meetingPoint
            + "'}";
    }
}
