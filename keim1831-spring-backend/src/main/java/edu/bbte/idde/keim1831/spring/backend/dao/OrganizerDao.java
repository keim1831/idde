package edu.bbte.idde.keim1831.spring.backend.dao;

import edu.bbte.idde.keim1831.spring.backend.model.Organizer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizerDao extends JpaRepository<Organizer, Integer> {

    List<Organizer> findByPhoneNumber(String phoneNumber);

}
