package edu.bbte.idde.keim1831.spring.backend.dao;

import edu.bbte.idde.keim1831.spring.backend.model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TripDao extends JpaRepository<Trip, Integer> {

    List<Trip> findByName(String name);

    @Query("select t.organizer.phoneNumber from Trip t where t.id = :id")
    String getOrganizersPhoneNumber(@Param("id") Integer id);

    @Query("select o.trips from Organizer o where o.id = :organizerId")
    List<Trip> selectTripsWithOrganizer(@Param("organizerId") Integer organizerId);

}
