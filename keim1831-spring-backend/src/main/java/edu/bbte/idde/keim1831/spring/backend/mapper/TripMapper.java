package edu.bbte.idde.keim1831.spring.backend.mapper;

import edu.bbte.idde.keim1831.spring.backend.dto.TripInDto;
import edu.bbte.idde.keim1831.spring.backend.dto.TripOutDto;
import edu.bbte.idde.keim1831.spring.backend.model.Trip;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class TripMapper {

    @IterableMapping(elementTargetType = TripOutDto.class)
    public abstract List<TripOutDto> modelsToDtos(Iterable<Trip> model);

    public abstract TripOutDto modelToDto(Trip model);

    public abstract Trip dtoToModel(TripInDto dto);

    public abstract void updateModel(TripInDto tripInDto, @MappingTarget Trip trip);

}