package edu.bbte.idde.keim1831.spring.backend.controller;

import edu.bbte.idde.keim1831.spring.backend.dao.TripDao;
import edu.bbte.idde.keim1831.spring.backend.dto.TripInDto;
import edu.bbte.idde.keim1831.spring.backend.mapper.TripMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

@Controller
@RequestMapping("/trips")
public class TripController {
    private static final Logger LOG = LoggerFactory.getLogger(TripController.class);

    @Autowired
    private TripDao tripDao;

    @Autowired
    private TripMapper tripMapper;

    @PostConstruct
    public void postConstruct() {
        LOG.info("Initializing TripController");
    }

    @GetMapping
    public ResponseEntity handleGet(@RequestParam(value = "name", required = false) String name) {

        if (name == null) {
            LOG.info("Handling GET all.");
            var trips = tripDao.findAll();
            return ResponseEntity.ok(tripMapper.modelsToDtos(trips));
        } else {
            LOG.info("Handling search by name.");
            var trips = tripDao.findByName(name);
            return ResponseEntity.ok(tripMapper.modelsToDtos(trips));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") Integer id) {
        var trip = tripDao.findById(id);
        if (!trip.isPresent()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(tripMapper.modelToDto(trip.get()));
        }
    }

    @GetMapping("/{id}/phone-number")
    public ResponseEntity getPhoneNumberForTrip(@PathVariable("id") Integer id) {
        var phoneNumber = tripDao.getOrganizersPhoneNumber(id);
        if (phoneNumber == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(phoneNumber);
        }
    }

    @PostMapping
    public ResponseEntity handlePost(@RequestBody @Valid TripInDto body) {
        LOG.info("Received valid POST with body {}", body);
        var trip = tripMapper.dtoToModel(body);
        trip.setId(0);
        tripDao.saveAndFlush(trip);
        return new ResponseEntity<>("Create was successful.", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity handlePut(@RequestBody @Valid TripInDto body, @PathVariable("id") Integer id) {
        var tripOpt = tripDao.findById(id);
        if (!tripOpt.isPresent()) {
            return new ResponseEntity<>("Trip NOT FOUND!", HttpStatus.NOT_FOUND);
        }
        var trip = tripOpt.get();
        LOG.info("Received valid PUT with body {}", body);

        tripMapper.updateModel(body, trip);

        tripDao.saveAndFlush(trip);
        return new ResponseEntity<>("Update was successful.", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity handleDelete(@PathVariable("id") Integer id) {
        var tripOpt = tripDao.findById(id);
        if (!tripOpt.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        LOG.info("Received valid DELETE {}", id);

        var trip = tripOpt.get();
        tripDao.delete(trip);
        tripDao.flush();
        return ResponseEntity.ok().build();
    }

}

