package edu.bbte.idde.keim1831.backend.dao.jdbc;

import edu.bbte.idde.keim1831.backend.dao.DaoException;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.model.Organizer;
import edu.bbte.idde.keim1831.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcTripDao implements TripDAO {

    private static Logger LOGGER = LoggerFactory.getLogger(JdbcTripDao.class);

    @Override
    public void create(Trip trip) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO trips (name, description, dateTime, meetingPoint, organizerId)"
                            + " VALUES (?, ?, ?, ?, ?)");
            preparedStatement.setString(1, trip.getName());
            preparedStatement.setString(2, trip.getDescription());
            preparedStatement.setTimestamp(3, new Timestamp(trip.getStart().getTime()));
            preparedStatement.setString(4, trip.getMeetingPoint());
            preparedStatement.setInt(5, trip.getOrganizer().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Could not create.");
            throw new DaoException("Error when creating", e);
        }

    }

    public Trip mapResultSetToTrip(ResultSet resultSet) throws SQLException {
        Trip trip = new Trip();
        trip.setId(resultSet.getInt("id"));
        trip.setName(resultSet.getString("name"));
        trip.setDescription(resultSet.getString("description"));
        trip.setStart(resultSet.getTimestamp("dateTime"));
        trip.setMeetingPoint(resultSet.getString("meetingPoint"));
        trip.setOrganizer(new Organizer(resultSet.getInt("organizerId"), null, null));
        return trip;
    }

    @Override
    public List<Trip> getAll() {
        try (Connection connection = DataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT id, name, description, dateTime, meetingPoint, organizerId FROM trips");
            List<Trip> trips = new ArrayList<>();
            while (resultSet.next()) {
                trips.add(mapResultSetToTrip(resultSet));

            }
            return trips;
        } catch (SQLException e) {
            LOGGER.error("Could not select.");
            throw new DaoException("Error when finding all trips", e);
        }
    }

    @Override
    public Trip getById(int id) throws EntityNotFoundException {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, name, description, dateTime, meetingPoint, organizerId FROM trips"
                            + " WHERE id = ?;");
            preparedStatement.setInt(1, id);
            var resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return mapResultSetToTrip(resultSet);
            } else {
                throw new EntityNotFoundException();
            }
        } catch (SQLException e) {
            LOGGER.error("Could not create.");
            throw new DaoException("Error when creating", e);
        }

    }

    @Override
    public void update(Trip trip) throws EntityNotFoundException {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE trips set name = ?, description = ?, dateTime = ?,"
                    + " meetingPoint = ?, organizerId = ? WHERE id = ?");
            preparedStatement.setString(1, trip.getName());
            preparedStatement.setString(2,trip.getDescription());
            preparedStatement.setTimestamp(3, new Timestamp(trip.getStart().getTime()));
            preparedStatement.setString(4, trip.getMeetingPoint());
            preparedStatement.setInt(5, trip.getOrganizer().getId());
            preparedStatement.setInt(6, trip.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Could not update.");
            throw new DaoException("Error when trying to update", e);
        }
    }

    @Override
    public void delete(int id) throws EntityNotFoundException {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM trips WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Could not delete.");
            throw new DaoException("Error when trying to delete trip from database.", e);
        }
    }

    @Override
    public List<Trip> selectByName(String search) {
        throw new DaoException("Not implemented.");
    }

    @Override
    public String getOrganizersPhoneNumber(int tripId) {
        throw new DaoException("Not implemented.");
    }
}
