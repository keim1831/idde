package edu.bbte.idde.keim1831.backend.dao.jpa;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.OrganizerDAO;
import edu.bbte.idde.keim1831.backend.model.Organizer;
import edu.bbte.idde.keim1831.backend.model.Trip;

import javax.persistence.TypedQuery;
import java.util.List;

public class JpaOrganizerDao extends AbstractJpaDao<Organizer> implements OrganizerDAO {
    public JpaOrganizerDao(String persistenceUnit) {
        super(Organizer.class, persistenceUnit);
    }

    @Override
    public List<Organizer> selectByPhoneNumber(String number) {
        final TypedQuery<Organizer> selectByPhoneNumber = entityManager.createNamedQuery(
                Organizer.PHONE_NUMBER_QUERY,
                Organizer.class);
        selectByPhoneNumber.setParameter("phoneNumber", number);
        return selectByPhoneNumber.getResultList();
    }

    @Override
    public List<Trip> selectTripsWithOrganizer(Integer id) throws EntityNotFoundException {
        var organizer = getById(id);
        return organizer.getTrips();
    }
}
