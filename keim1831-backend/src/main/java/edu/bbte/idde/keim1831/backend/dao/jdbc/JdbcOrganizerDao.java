package edu.bbte.idde.keim1831.backend.dao.jdbc;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.DaoException;
import edu.bbte.idde.keim1831.backend.dao.OrganizerDAO;
import edu.bbte.idde.keim1831.backend.model.Organizer;
import edu.bbte.idde.keim1831.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcOrganizerDao implements OrganizerDAO {
    private static Logger LOGGER = LoggerFactory.getLogger(JdbcOrganizerDao.class);

    @Override
    public void create(Organizer organizer) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO organizers (name, phoneNumber)"
                            + " VALUES (?, ?)");
            preparedStatement.setString(1, organizer.getName());
            preparedStatement.setString(2, organizer.getPhoneNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Could not create.");
            throw new DaoException("Error when creating", e);
        }
    }

    public Organizer mapResultSetToOrganizer(ResultSet resultSet) throws SQLException {
        Organizer organizer = new Organizer();
        organizer.setId(resultSet.getInt(1));
        organizer.setName(resultSet.getString(2));
        organizer.setPhoneNumber(resultSet.getString(3));
        return organizer;
    }

    @Override
    public List<Organizer> getAll() {
        try (Connection connection = DataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT id, name, phoneNumber FROM organizers");
            List<Organizer> organizers = new ArrayList<>();
            while (resultSet.next()) {
                organizers.add(mapResultSetToOrganizer(resultSet));
            }
            return organizers;
        } catch (SQLException e) {
            LOGGER.error("Could not select.");
            throw new DaoException("Error when finding all organizer", e);
        }
    }

    @Override
    public Organizer getById(int id) throws EntityNotFoundException {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, name, phoneNumber FROM organizers WHERE id = ?;");
            preparedStatement.setInt(1, id);
            var resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return mapResultSetToOrganizer(resultSet);
            } else {
                throw new EntityNotFoundException();
            }
        } catch (SQLException e) {
            LOGGER.error("Could not create.");
            throw new DaoException("Error when creating", e);
        }
    }

    @Override
    public void update(Organizer organizer) throws EntityNotFoundException {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE organizers set name = ?, phoneNumber = ? WHERE id = ?");
            preparedStatement.setString(1, organizer.getName());
            preparedStatement.setString(2, organizer.getPhoneNumber());
            preparedStatement.setInt(3, organizer.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Could not update.");
            throw new DaoException("Error when trying to update", e);
        }
    }

    @Override
    public void delete(int id) throws EntityNotFoundException {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM organizers WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Could not delete.");
            throw new DaoException("Error when trying to delete organizer from database.", e);
        }
    }

    @Override
    public List<Organizer> selectByPhoneNumber(String number) {
        throw new DaoException("Not implemented.");
    }

    @Override
    public List<Trip> selectTripsWithOrganizer(Integer id) {
        throw new DaoException("Not implemented.");
    }
}
