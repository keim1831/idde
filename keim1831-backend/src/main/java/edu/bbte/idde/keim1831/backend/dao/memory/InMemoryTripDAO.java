package edu.bbte.idde.keim1831.backend.dao.memory;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.DaoException;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.model.Organizer;
import edu.bbte.idde.keim1831.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class InMemoryTripDAO implements TripDAO {

    private final List<Trip> db = new ArrayList<>();
    private final Random rnd = new Random();
    private static Logger LOGGER = LoggerFactory.getLogger(InMemoryTripDAO.class);

    public InMemoryTripDAO() {
        this.create(new Trip(0, "Name1", "desc1", new Date(2020, Calendar.DECEMBER,12),
                "meetingPoint1"));
        this.create(new Trip(0, "Name2", "desc2", new Date(2020, Calendar.NOVEMBER,12),
                "meetinPoint2"));
        this.create(new Trip(0, "Name3", "desc3", new Date(2020, Calendar.DECEMBER,15),
                "meetingPoint3"));
    }

    @Override
    public void create(Trip trip) {
        trip.setId(rnd.nextInt(10000));
        db.add(trip);
        LOGGER.info("The " + trip.getName() + " trip was created.");
    }

    @Override
    public List<Trip> getAll() {
        LOGGER.info("The informations about trips was retrieved.");
        return new ArrayList<>(db);
    }

    @Override
    public Trip getById(int id) throws EntityNotFoundException {
        for (var t : db) {
            if (t.getId() == id) {
                LOGGER.info("The " + t.getName() + " trip was retrieved.");
                return t;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void update(Trip trip) throws EntityNotFoundException {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).getId() == trip.getId()) {
                db.set(i, trip);
                LOGGER.info("The " + trip.getName() + " trip was updated.");
                return;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void delete(int id) throws EntityNotFoundException {
        for (var t: db) {
            if (t.getId() == id) {
                db.remove(t);
                LOGGER.info("The " + t.getName() + " trip was removed.");
                return;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public List<Trip> selectByName(String search) {
        throw new DaoException("Not implemented.");
    }

    @Override
    public String getOrganizersPhoneNumber(int tripId) {
        throw new DaoException("Not implemented.");
    }
}
