package edu.bbte.idde.keim1831.backend.config;

import java.io.Serializable;

public class Config implements Serializable {

    private String daoType;
    private String driver;
    private String jdbcUrl;
    private String userName;
    private String password;
    private String persistenceUnit;
    private int maximumPoolSize;

    public String getDaoType() {
        return daoType;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public void setDaoType(String daoType) {
        this.daoType = daoType;
    }

    public String getPersistenceUnit() {
        return persistenceUnit;
    }

    public void setPersistenceUnit(String persistenceUnit) {
        this.persistenceUnit = persistenceUnit;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Config{");
        sb.append("daoType='").append(daoType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
