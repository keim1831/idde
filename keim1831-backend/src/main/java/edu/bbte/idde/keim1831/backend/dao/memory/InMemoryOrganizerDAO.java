package edu.bbte.idde.keim1831.backend.dao.memory;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.DaoException;
import edu.bbte.idde.keim1831.backend.dao.OrganizerDAO;
import edu.bbte.idde.keim1831.backend.model.Organizer;
import edu.bbte.idde.keim1831.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InMemoryOrganizerDAO implements OrganizerDAO {

    private final List<Organizer> db = new ArrayList<>();
    private final Random rnd = new Random();
    private static Logger LOGGER = LoggerFactory.getLogger(InMemoryOrganizerDAO.class);

    @Override
    public void create(Organizer organizer) {
        organizer.setId(rnd.nextInt(10000));
        db.add(organizer);
        LOGGER.info("The " + organizer.getName() + " organizer was added.");
    }

    @Override
    public List<Organizer> getAll() {
        LOGGER.info("The informations about organizers was retrieved.");
        return new ArrayList<>(db);
    }

    @Override
    public Organizer getById(int id) throws EntityNotFoundException {
        for (var o : db) {
            if (o.getId() == id) {
                LOGGER.info("The " + o.getName() + " organizer was retrieved.");
                return o;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void update(Organizer organizer) throws EntityNotFoundException {
        for (int i = 0; i < db.size(); i++) {
            if (db.get(i).getId() == organizer.getId()) {
                db.set(i, organizer);
                LOGGER.info("The " + organizer.getName() + " trip was updated.");
                return;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void delete(int id) throws EntityNotFoundException {
        for (var o : db) {
            if (o.getId() == id) {
                db.remove(o);
                LOGGER.info("The " + o.getName() + " organazer was removed.");
                return;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public List<Organizer> selectByPhoneNumber(String number) {
        throw new DaoException("Not implemented.");
    }

    @Override
    public List<Trip> selectTripsWithOrganizer(Integer id) {
        throw new DaoException("Not implemented.");
    }
}
