package edu.bbte.idde.keim1831.backend.dao.jpa;

import edu.bbte.idde.keim1831.backend.dao.DaoFactory;
import edu.bbte.idde.keim1831.backend.dao.OrganizerDAO;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;

public class JpaDaoFactory extends DaoFactory {

    private final JpaTripDao jpaTripDao;
    private final JpaOrganizerDao jpaOrganizerDao;

    public JpaDaoFactory(String persistenceUnit) {
        jpaTripDao = new JpaTripDao(persistenceUnit);
        jpaOrganizerDao = new JpaOrganizerDao(persistenceUnit);
    }

    @Override
    public TripDAO getTripDao() {
        return jpaTripDao;
    }

    @Override
    public OrganizerDAO getOrganizerDao() {
        return jpaOrganizerDao;
    }
}
