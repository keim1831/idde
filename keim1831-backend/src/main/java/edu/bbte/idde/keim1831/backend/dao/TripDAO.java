package edu.bbte.idde.keim1831.backend.dao;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.model.Trip;

import java.util.List;

public interface TripDAO extends Dao<Trip> {

    List<Trip> selectByName(String search);

    String getOrganizersPhoneNumber(int tripId) throws EntityNotFoundException;

}