package edu.bbte.idde.keim1831.backend.dao.jpa;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.Dao;
import edu.bbte.idde.keim1831.backend.dao.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import java.util.List;

public abstract class AbstractJpaDao<T> implements Dao<T> {

    protected EntityManager entityManager;
    private Class<T> clazz;
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJpaDao.class);

    public AbstractJpaDao(Class<T> clazz, String persistenceUnitName) {
        entityManager = Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
        this.clazz = clazz;
    }

    @Override
    public void create(T item) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        try {
            entityManager.persist(item);
            entityTransaction.commit();
            LOGGER.info("Entity created.");
        } catch (PersistenceException e) {
            entityTransaction.rollback();
            LOGGER.info("Trnsaction failed.");
            throw new DaoException("Create failed", e);
        }
    }

    @Override
    public List<T> getAll() {
        LOGGER.info("Select entities.");
        return  entityManager.createQuery("FROM " + clazz.getSimpleName(), clazz).getResultList();
    }

    @Override
    public T getById(int id) throws EntityNotFoundException {
        LOGGER.info("Select an entity by id.");
        return entityManager.find(clazz, id);
    }

    @Override
    public void update(T t) throws EntityNotFoundException {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        try {
            entityManager.merge(t);
            entityTransaction.commit();
            LOGGER.info("Entity updated.");
        } catch (PersistenceException e) {
            entityTransaction.rollback();
            LOGGER.info("Transaction failed.");
            throw new DaoException("Update failed", e);
        }
    }

    @Override
    public void delete(int id) throws EntityNotFoundException {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        try {
            var entity = getById(id);
            if (entity != null) {
                entityManager.remove(entity);
                entityTransaction.commit();
                LOGGER.info("Entity deleted.");
            } else  {
                LOGGER.info("Entity not found.");
            }
        } catch (PersistenceException e) {
            entityTransaction.rollback();
            LOGGER.info("Transaction failed.");
            throw new DaoException("Delete failed", e);
        }
    }
}
