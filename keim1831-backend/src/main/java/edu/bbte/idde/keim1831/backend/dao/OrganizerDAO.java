package edu.bbte.idde.keim1831.backend.dao;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.model.Organizer;
import edu.bbte.idde.keim1831.backend.model.Trip;

import java.util.List;

public interface OrganizerDAO extends Dao<Organizer> {

    List<Organizer> selectByPhoneNumber(String number);

    List<Trip> selectTripsWithOrganizer(Integer id) throws EntityNotFoundException;

}
