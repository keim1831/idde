package edu.bbte.idde.keim1831.backend.dao.jpa;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.model.Trip;

import javax.persistence.TypedQuery;
import java.util.List;

public class JpaTripDao extends AbstractJpaDao<Trip> implements TripDAO {

    public JpaTripDao(String persistenceUnit) {
        super(Trip.class, persistenceUnit);
    }

    @Override
    public List<Trip> selectByName(String search) {
        final TypedQuery<Trip> selectByName = entityManager.createNamedQuery(Trip.TRIP_NAME_QUERY, Trip.class);
        selectByName.setParameter("name", "%" + search + "%");
        return selectByName.getResultList();
    }

    @Override
    public String getOrganizersPhoneNumber(int tripId) throws EntityNotFoundException {
        var trip = getById(tripId);
        return trip.getOrganizer().getPhoneNumber();
    }
}
