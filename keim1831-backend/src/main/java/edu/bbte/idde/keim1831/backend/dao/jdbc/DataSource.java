package edu.bbte.idde.keim1831.backend.dao.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.keim1831.backend.config.ConfigurationFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static HikariDataSource dataSource;

    public static synchronized Connection getConnection() throws SQLException {
        if (dataSource == null) {
            dataSource = createDataSource();
        }
        return dataSource.getConnection();
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        var config = ConfigurationFactory.getMainConfiguration();
        hikariConfig.setDriverClassName(config.getDriver());
        hikariConfig.setJdbcUrl(config.getJdbcUrl());
        hikariConfig.setUsername(config.getUserName());
        hikariConfig.setPassword(config.getPassword());
        hikariConfig.setMaximumPoolSize(config.getMaximumPoolSize());
        return new HikariDataSource(hikariConfig);
    }
}
