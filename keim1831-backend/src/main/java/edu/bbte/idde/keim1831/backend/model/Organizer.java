package edu.bbte.idde.keim1831.backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "organizers")
@NamedQuery(name = Organizer.PHONE_NUMBER_QUERY, query = "FROM Organizer WHERE phoneNumber = :phoneNumber")
public class Organizer extends BaseEntity {

    public static final String PHONE_NUMBER_QUERY = "selectByOrganizerPhoneNumber";

    @Column(length = 50)
    private String name;
    @Column(length = 12)
    private String phoneNumber;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "organizerId", referencedColumnName = "id")
    private List<Trip> trips = new ArrayList<>();

    public Organizer() {
    }

    public Organizer(int id, String name, String phoneNumber) {
        super(id);
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Organizer{");
        sb.append("name='").append(name).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
