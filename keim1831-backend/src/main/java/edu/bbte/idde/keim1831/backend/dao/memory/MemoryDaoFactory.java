package edu.bbte.idde.keim1831.backend.dao.memory;

import edu.bbte.idde.keim1831.backend.dao.OrganizerDAO;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.dao.DaoFactory;

public class MemoryDaoFactory extends DaoFactory {

    public static final InMemoryTripDAO IN_MEMORY_TRIP_DAO = new InMemoryTripDAO();
    public static final InMemoryOrganizerDAO IN_MEMORY_ORGANIZER_DAO = new InMemoryOrganizerDAO();

    @Override
    public TripDAO getTripDao() {
        return IN_MEMORY_TRIP_DAO;
    }

    @Override
    public OrganizerDAO getOrganizerDao() {
        return IN_MEMORY_ORGANIZER_DAO;
    }
}
