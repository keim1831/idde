package edu.bbte.idde.keim1831.backend.dao.jdbc;

import edu.bbte.idde.keim1831.backend.dao.OrganizerDAO;
import edu.bbte.idde.keim1831.backend.dao.TripDAO;
import edu.bbte.idde.keim1831.backend.dao.DaoFactory;

public class JdbcDaoFactory extends DaoFactory {

    private static final JdbcTripDao JDBC_TRIP_DAO = new JdbcTripDao();
    private static final JdbcOrganizerDao JDBC_ORGANIZER_DAO = new JdbcOrganizerDao();

    @Override
    public TripDAO getTripDao() {
        return JDBC_TRIP_DAO;
    }

    @Override
    public OrganizerDAO getOrganizerDao() {
        return JDBC_ORGANIZER_DAO;
    }
}
