package edu.bbte.idde.keim1831.backend.dao;

import edu.bbte.idde.keim1831.backend.EntityNotFoundException;

import java.util.List;

public interface Dao<T> {
    void create(T t);

    List<T> getAll();

    T getById(int id) throws EntityNotFoundException;

    void update(T t) throws EntityNotFoundException;

    void delete(int id) throws EntityNotFoundException;
}
