package edu.bbte.idde.keim1831.backend.dao;

import edu.bbte.idde.keim1831.backend.config.Config;
import edu.bbte.idde.keim1831.backend.config.ConfigurationFactory;
import edu.bbte.idde.keim1831.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.keim1831.backend.dao.jpa.JpaDaoFactory;
import edu.bbte.idde.keim1831.backend.dao.memory.MemoryDaoFactory;

public abstract class DaoFactory  {

    private static DaoFactory INSTANCE;

    public abstract TripDAO getTripDao();

    public abstract OrganizerDAO getOrganizerDao();

    public static synchronized DaoFactory getInstance() {
        if (INSTANCE == null) {
            Config config =  ConfigurationFactory.getMainConfiguration();
            String daoType = config.getDaoType();
            if ("mem".equals(daoType)) {
                INSTANCE = new MemoryDaoFactory();
            } else if ("jdbc".equals(daoType)) {
                INSTANCE = new JdbcDaoFactory();
            } else if ("jpa".equals(daoType)) {
                INSTANCE = new JpaDaoFactory(config.getPersistenceUnit());
            } else {
                throw new  DaoException("Could not read config file.".concat(daoType));
            }
        }
        return INSTANCE;
    }
}
