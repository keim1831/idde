package edu.bbte.idde.keim1831.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigurationFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationFactory.class);
    private static final String DEFAULF_CONFIG_FILE = "config-jpa.json";
    //private static final String DEFAULF_CONFIG_FILE = "config-jdbc.json";

    // A Jackson JSON olvasó objektuma
    private static ObjectMapper objectMapper;

    private static Config mainConfiguration = new Config();

    private static final String CONFIG_FILE_ENV = "CONFIG_FILE";

    static {
        // JSON olvasó inicializálása
        objectMapper = new ObjectMapper();

        String configFile = System.getProperty(CONFIG_FILE_ENV);

        if (configFile == null || configFile.isEmpty()) {
            configFile = System.getenv(CONFIG_FILE_ENV);
        }

        if (configFile == null || configFile.isEmpty()) {
            configFile = DEFAULF_CONFIG_FILE;
        }

        // kérünk olvasási streamet a JSON állományhoz
        try (InputStream inputStream = ConfigurationFactory.class.getResourceAsStream("/" + configFile)) {
            // JSON állomány beolvasása az általunk megadott osztály egy példányába
            mainConfiguration = objectMapper.readValue(inputStream, Config.class);
            LOG.info("Read following configuration: {} - {}", mainConfiguration, configFile);
        } catch (IOException e) {
            LOG.error("Error loading configuration", e);
        }
    }

    public static Config getMainConfiguration() {
        return mainConfiguration;
    }

}

